package com.estragon.linq.dao;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.estragon.linq.Appli;
import com.estragon.linq.core.Partie;
import com.google.gson.Gson;

public class PartieDAO {

	
	public static void persist(Partie partie) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Appli.appli);
		prefs.edit().putString("partie", new Gson().toJson(partie)).commit();
	}
	
	public static Partie getPartie() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Appli.appli);
		String partie = prefs.getString("partie", null);
		if (partie == null) {
			Partie partie2 = new Partie();
			persist(partie2);
			return partie2;
		}
		
		return new Gson().fromJson(partie, Partie.class);
	}
	
	public static void reset() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Appli.appli);
		prefs.edit().putString("partie", null).commit();
	}
}
