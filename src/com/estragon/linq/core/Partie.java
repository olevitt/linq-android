package com.estragon.linq.core;

import java.util.ArrayList;
import java.util.List;

import com.estragon.linq.SMSReceiver;

import android.text.TextUtils;

public class Partie {

	public static int ETAT_ATTENTE = 0, ETAT_ENCOURS = 1, ETAT_FINI = 2;
	
	
	private List<Joueur> joueurs = new ArrayList<Joueur>();
	
	String[] motsPossibles = new String[] {"brosse", "arbre", "branche", "cerise", "brebis", "cahier", "savon", "zèbre"};
	
	private int etat = ETAT_ATTENTE;
	
	public Partie() {
		
	}
	
	public boolean ajouterJoueur(User user) {
		if (joueurs.size() >= 4) {
			return false;
		}
		
		if (getJoueurByNumero(user.getNumero()) != null) {
			return false;
		}
		
		joueurs.add(new Joueur(user));
		return true;
	}
	
	public Joueur getJoueurByNumero(String numero) {
		for (Joueur joueur : joueurs) {
			if (TextUtils.equals(joueur.getUser().getNumero(),numero)) {
				return joueur;
			}
		}
		return null;
	}
	
	public void commencerPartie() {
		if (joueurs.size() != 4) {
			return;
		}
		
		etat = ETAT_ENCOURS;
		
		int random = (int) (Math.random() * joueurs.size());
		int random2 = (int) (Math.random() * joueurs.size());
		while (random2 == random) {
			random2 = (int) (Math.random() * joueurs.size());
		}
		
		String mot = motsPossibles[(int) (Math.random() * motsPossibles.length)];
		
		joueurs.get(random).setMot(mot);
		joueurs.get(random2).setMot(mot);
		
		for (Joueur joueur : joueurs) {
			envoyerMot(joueur);
		}
		
		for (Joueur joueur : joueurs) {
			envoyerBriefing(joueur);
		}
	}
	
	private void envoyerBriefing(Joueur joueur) {
		String texte = "La partie commence, voici l'ordre : ";
		for (Joueur joueurCourant : joueurs) {
			texte += joueurCourant.getUser().getPseudo()+ " ";
		}
		texte += ".En attente de "+joueurs.get(0);
		
		SMSReceiver.envoyerSMS(joueur.getUser().getNumero(), texte);
	}
	
	private void envoyerMot(Joueur joueur) {
		String texte = "Pour cette partie, tu es contre-espion, bonne chance !";
		if (joueur.getMot() != null) {
			texte = "Le mot espion pour cette partie est \""+joueur.getMot()+"\". Bonne chance !";
		}
		
		SMSReceiver.envoyerSMS(joueur.getUser().getNumero(), texte);
	}
	
	public void nouveauMot(String mot) {
		Joueur joueur = getJoueurActif();
		if (joueur == null) {
			return;
		}
		
		if (joueur.getMotsDits()[0] == null) {
			joueur.getMotsDits()[0] = mot;
		}
		else {
			joueur.getMotsDits()[1] = mot;
		}
		
		String message = "";
		if (joueur.getMotsDits()[1] == null) {
			message = joueur.getUser().getPseudo()+" dit "+mot;
		}
		else {
			message = joueur.getUser().getPseudo()+" avait dit "+joueur.getMotsDits()[0]+" et dit "+joueur.getMotsDits()[1];
		}
		
		Joueur nouveauJoueurActif = getJoueurActif();
		if (nouveauJoueurActif == null) {
			message += ".Phase des mots terminée !";
		}
		message += ".En attente de "+nouveauJoueurActif.getUser().getPseudo();
		broadCast(message);
	}
	
	public void broadCast(String message) {
		for (Joueur joueur : joueurs) {
			SMSReceiver.envoyerSMS(joueur.getUser().getNumero(), message);
		}
	}

	public List<Joueur> getJoueurs() {
		return joueurs;
	}
	
	public Joueur getJoueurActif() {
		if (etat != ETAT_ENCOURS) {
			return null;
		}
		
		for (Joueur joueur : joueurs) {
			if (joueur.motsDits[0] == null) {
				return joueur;
			}
		}
		
		for (Joueur joueur : joueurs) {
			if (joueur.motsDits[1] == null) {
				return joueur;
			}
		}
		
		return null;
	}
	
	
}
