package com.estragon.linq.core;

public class User {

	String numero;
	String pseudo;
	
	public User(String numero,String pseudo) {
		this.numero = numero;
		this.pseudo = pseudo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
	
}
