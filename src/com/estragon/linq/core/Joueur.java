package com.estragon.linq.core;

public class Joueur {

	User user;
	String mot;
	String[] motsDits = new String[] {null, null};
	
	public Joueur(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMot() {
		return mot;
	}

	public void setMot(String mot) {
		this.mot = mot;
	}

	public String[] getMotsDits() {
		return motsDits;
	}
	
	
	
}
