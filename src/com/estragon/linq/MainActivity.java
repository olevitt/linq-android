package com.estragon.linq;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import android.widget.Toast;

import com.estragon.linq.core.Joueur;
import com.estragon.linq.core.Partie;
import com.estragon.linq.dao.PartieDAO;

public class MainActivity extends Activity implements OnSharedPreferenceChangeListener {

	Partie partie;
	TextView textPartie;
	Button reset;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		textPartie = (TextView) findViewById(R.id.textpartie);
		reset = (Button) findViewById(R.id.reset);
		
		PreferenceManager.getDefaultSharedPreferences(Appli.appli).registerOnSharedPreferenceChangeListener(this);
		chargerPartie(PartieDAO.getPartie());
	}
	
	public void chargerPartie(Partie partie) {
		textPartie.setText("");
		this.partie = partie;
		for (Joueur joueur : partie.getJoueurs()) {
			textPartie.setText(textPartie.getText()+joueur.getUser().getPseudo()+System.getProperty("line.separator"));
		}
	}
	
	public void reset(View v) {
		partie.broadCast("La partie a été annulée par le grand manitou");
		PartieDAO.reset();
	}
	

	@Override
	protected void onDestroy() {
		PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
		super.onDestroy();
	}




	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}



	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (!"partie".equals(key)) {
			return;
		}
		
		chargerPartie(PartieDAO.getPartie());
	}

}
