package com.estragon.linq;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

import com.estragon.linq.core.Joueur;
import com.estragon.linq.core.Partie;
import com.estragon.linq.core.User;
import com.estragon.linq.dao.PartieDAO;

public class SMSReceiver extends BroadcastReceiver {

	public static final String SMS_EXTRA_NAME = "pdus";

	public void onReceive( Context context, Intent intent ) 
	{
		// Get the SMS map from Intent
		Bundle extras = intent.getExtras();

		String messages = "";

		if ( extras != null )
		{
			// Get received SMS array
			Object[] smsExtra = (Object[]) extras.get( SMS_EXTRA_NAME );

			// Get ContentResolver object for pushing encrypted SMS to the incoming folder
			ContentResolver contentResolver = context.getContentResolver();


			for ( int i = 0; i < smsExtra.length; ++i )
			{
				SmsMessage sms = SmsMessage.createFromPdu((byte[])smsExtra[i]);

				String body = sms.getMessageBody().toString();
				String address = sms.getOriginatingAddress();

				messages += "SMS from " + address + " :\n";                    
				messages += body + "\n";

				// Here you can add any your code to work with incoming SMS
				// I added encrypting of all received SMS 
				
				smsRecu(address, body);
			}
			
			
		}

		// WARNING!!! 
		// If you uncomment the next line then received SMS will not be put to incoming.
		// Be careful!
		// this.abortBroadcast(); 
	}

	public void smsRecu(String from, String message) {
		if (message == null) {
			return;
		}
		
		Partie partie = PartieDAO.getPartie();
		
		Joueur joueur = partie.getJoueurByNumero(from);
		
		String[] split = message.split(" ");
		if (split.length == 0) {
			return;
		}

		if (split[0].equalsIgnoreCase("help") || split[0].equalsIgnoreCase("aide")) {
			String texte = "--Aide : jouer [\"pseudo\"] : commencer à jouer.";
			texte += "pseudo \"pseudo\" : changer de pseudo.";
			texte += "mot \"mot\" : envoyer un mot.";
			envoyerSMS(from, texte);
		}
		else if (split[0].equalsIgnoreCase("jouer")) {
			String pseudo = "joueur"+(partie.getJoueurs().size()+1);
			if (split.length > 1) {
				pseudo = split[1];
			}
			User user = new User(from, pseudo);
			boolean succes = partie.ajouterJoueur(user);
			if (succes) {
				envoyerSMS(from, "Merci "+pseudo+", inscription validée.Il y a "+partie.getJoueurs().size()+" joueurs pour le moment");
			}
			else {
				envoyerSMS(from, "Erreur lors de l'inscription");
			}
			
			if (partie.getJoueurs().size() == 4) {
				partie.commencerPartie();
			}
		}
		else if (split[0].equalsIgnoreCase("pseudo")) {
			if (joueur == null) {
				envoyerSMS(from, "La commande pseudo n'est disponible qu'une fois inscrit à une partie");
			}
			else if (split.length > 1) {
				joueur.getUser().setPseudo(split[1]);
				envoyerSMS(from, "Changement de pseudo validé, nouveau pseudo : "+split[1]);
			}
			else {
				envoyerSMS(from, "Erreur, usage pseudo xxx");
			}
		}
		else if (split[0].equalsIgnoreCase("mot")) {
			if (split.length != 2) {
				envoyerSMS(from, "Erreur, l'instruction mot doit être suivie d'un mot unique");
			}
			else {
				Joueur joueurActif = partie.getJoueurActif();
				if (joueur == null) {
					envoyerSMS(from, "Erreur, vous ne faites pas partie de la partie.");
				}
				else if (joueurActif == null) {
					envoyerSMS(from, "Erreur, la partie n'est pas en cours de jeu.");
				}
				else if (!joueurActif.getUser().getNumero().equals(from)) {
					envoyerSMS(from, "Erreur, ce n'est pas votre tour !");
				}
				else {
					partie.nouveauMot(split[1]);
				}
			}
		}
		
		PartieDAO.persist(partie);
	}

	public static void envoyerSMS(String to, String contenu) {
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(to, null, contenu, null, null);
	}



}
